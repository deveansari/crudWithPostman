const express = require('express');
const bodyParser = require('body-parser');
// better alternative than string concatination in joining path
const path = require('path');

//declare route 
const api = require('./server/routes/api');
const port = 3000;
//creating instance of express
const app = express();
//give express to access distibutable folder (all the angular code have placed )
app.use(express.static(path.join(__dirname, 'dist')));
//parse text to url encoded data
app.use(bodyParser.urlencoded({extended: true}));  
//parse data to json means json like experience
app.use(bodyParser.json()); 

//teach express when to use it
app.use('/api', api);

//any request redirect to index.html in dist folder
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

app.listen(port, function(){
    console.log("Server running on localhost:" + port);
});