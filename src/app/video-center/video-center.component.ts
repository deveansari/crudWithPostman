import { Video } from './../video';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video-center',
  templateUrl: './video-center.component.html',
  styleUrls: ['./video-center.component.css']
})
export class VideoCenterComponent implements OnInit {
    videos: Video[] = [
        {"_id": "1", "title": "Tittle 1", "url": "url 1", "description": "description 1"},
        {"_id": "2", "title": "Tittle 2", "url": "url 2", "description": "description 2"},
        {"_id": "3", "title": "Tittle 3", "url": "url 3", "description": "description 3"},
        {"_id": "4", "title": "Tittle 4", "url": "url 4", "description": "description 4"}
    ];
    selectedVideo: Video;
  constructor() {}

  ngOnInit() {
  }

    onSelectVideo(video: any){
    this.selectedVideo = video;
    console.log(this.selectedVideo);
    }
}
